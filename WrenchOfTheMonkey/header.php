<?php global $wp_locale;
if (isset($wp_locale)) {
	$wp_locale->text_direction = 'ltr';
} ?>
<!DOCTYPE html>
<html itemscope itemtype="https://schema.org/WebSite" <?php language_attributes(); ?>>
<head itemtype="https://schema.org/WPHeader">
<meta charset="<?php bloginfo('charset') ?>" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<!-- Created by Wrench of the Monkey -->
<meta name="viewport" content="initial-scale = 1.0, maximum-scale = 1.0, user-scalable = no, width = device-width" />
<!--[if lt IE 9]><script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url') ?>" media="screen" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php
remove_action('wp_head', 'wp_generator');
if (is_singular() && get_option('thread_comments')) {
	wp_enqueue_script('comment-reply');
}
wp_head();
?>
</head>
<body itemscope itemtype="https://schema.org/WebPage" <?php body_class(); ?>>
<?php include_once("analyticstracking.php") ?>

<div id="wotm-main">

<?php if(theme_has_layout_part("header")) : ?>
<header class="wotm-header<?php echo (theme_get_option('theme_header_clickable') ? ' clickable' : ''); ?>"><?php get_sidebar('header'); ?></header>
<?php endif; ?>

<div class="wotm-sheet clearfix">
            <div class="wotm-layout-wrapper">
                <div class="wotm-content-layout">
                    <div class="wotm-content-layout-row">
                        <div class="wotm-layout-cell wotm-content">
<?php if ( function_exists('yoast_breadcrumb') ) {
	yoast_breadcrumb('<p id="breadcrumbs">','</p>');
} ?>
