<?php
global $theme_sidebars;
$places = array();
foreach ($theme_sidebars as $sidebar){
    if ($sidebar['group'] !== 'footer')
        continue;
    $widgets = theme_get_dynamic_sidebar_data($sidebar['id']);
    if (!is_array($widgets) || count($widgets) < 1)
        continue;
    $places[$sidebar['id']] = $widgets;
}
$place_count = count($places);
$needLayout = ($place_count > 1);
if (theme_get_option('theme_override_default_footer_content')) {
    if ($place_count > 0) {
        $centred_begin = '<div class="wotm-center-wrapper"><div class="wotm-center-inner">';
        $centred_end = '</div></div><div class="clearfix"> </div>';
        if ($needLayout) { ?>
<div class="wotm-content-layout">
    <div class="wotm-content-layout-row">
        <?php 
        }
        foreach ($places as $widgets) { 
            if ($needLayout) { ?>
            <div class="wotm-layout-cell wotm-layout-cell-size<?php echo $place_count; ?>">
            <?php 
            }
            $centred = false;
            foreach ($widgets as $widget) {
                 $is_simple = ('simple' == $widget['style']);
                 if ($is_simple) {
                     $widget['class'] = implode(' ', array_merge(explode(' ', theme_get_array_value($widget, 'class', '')), array('wotm-footer-text')));
                 }
                 if (false === $centred && $is_simple) {
                     $centred = true;
                     echo $centred_begin;
                 }
                 if (true === $centred && !$is_simple) {
                     $centred = false;
                     echo $centred_end;
                 }
                 theme_print_widget($widget);
            } 
            if (true === $centred) {
                echo $centred_end;
            }
            if ($needLayout) {
           ?>
            </div>
        <?php 
            }
        } 
        if ($needLayout) { ?>
    </div>
</div>
        <?php 
        }
    }
?>
<div class="wotm-footer-text">
<?php
global $theme_default_options;
echo do_shortcode(theme_get_option('theme_override_default_footer_content') ? theme_get_option('theme_footer_content') : theme_get_array_value($theme_default_options, 'theme_footer_content'));
} else { 
?>
<div class="wotm-footer-text">
<?php theme_ob_start() ?>
  
<p></p><p></p><div itemscope="" itemtype="https://schema.org/WPFooter" style="position:relative;display:inline-block;padding-left:40px;padding-right:40px"><?php theme_ob_start() ?><a itemprop="url" title="RSS" class="wotm-rss-tag-icon" style="position: absolute; bottom: -10px; left: -6px; line-height: 50px; " href="#"></a><?php echo do_shortcode(theme_ob_get_clean()) ?><a itemprop="url" title="Linkedin" class="wotm-linkedin-tag-icon" style="position: absolute; bottom: -10px; right: -6px; line-height: 32px;" href="http://www.linkedin.com/company/wrench-of-the-monkey-llc/"></a>
<p>&nbsp;<a itemprop="url" href="https://www.facebook.com/wrenchofthemonkey" target="_blank" title="Facebook" class="wotm-facebook-tag-icon" style="line-height: 32px; "></a>&nbsp;|&nbsp;&nbsp;<a itemprop="url" href="https://twitter.com/WrenchOfMonkey" target="_blank" title="Twitter" class="wotm-twitter-tag-icon" style="line-height: 32px;"></a>&nbsp;| &nbsp;<a itemprop="url" href="https://plus.google.com/+WrenchofthemonkeyLLC/" target="_blank" class="wotm-tumblr-tag-icon"></a>&nbsp;&nbsp;|&nbsp;</p><p>
</p><p><name itemprop="copyrightHolder">Wrench of the Monkey LLC</name> © <year itemprop="copyrightYear">2013</year>. All Rights Reserved.&nbsp;<br /><a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/privacy-policy']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_blank">Privacy Policy</a>&nbsp;| <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/terms-of-use']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_blank">Terms of Use</a>&nbsp;| <a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/term-of-sales']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_blank">Term of Sales</a>&nbsp;|&nbsp;<a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/order-verification']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_blank">Order Verification</a> |&nbsp;<a itemprop="url" href="<?php theme_ob_start() ?>[post_link name='/return-policy']<?php echo do_shortcode(theme_ob_get_clean()) ?>" target="_blank">Return Policy</a>&nbsp; &nbsp; &nbsp;</p>
</div><p></p><p>
    </p>
    
  
<?php echo do_shortcode(theme_ob_get_clean()) ?>
<?php } ?>
<p class="wotm-page-footer">
        <span id="wotm-footnote-links">Designed by <a href="http://www.wrenchofthemonkey.com" target="_blank">Wrench of the Monkey</a>.</span>
    </p>
</div>
