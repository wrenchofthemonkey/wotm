<?php theme_print_sidebar('header-widget-area'); ?>


    <div class="wotm-shapes">
        <div class="wotm-object0"></div>
<div class="wotm-textblock wotm-object7998138">
        <div class="wotm-object7998138-text-container">
        <div class="wotm-object7998138-text"><p class="MsoNormal">FROM TRUE PROFESSIONALS</p></div>
    </div>
    
</div>
            </div>

<?php if(theme_get_option('theme_header_show_headline')): ?>
	<?php $headline = theme_get_option('theme_'.(is_home()?'posts':'single').'_headline_tag'); ?>
	<<?php echo $headline; ?> class="wotm-headline">
    <a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a>
</<?php echo $headline; ?>>
<?php endif; ?>
<?php if(theme_get_option('theme_header_show_slogan')): ?>
	<?php $slogan = theme_get_option('theme_'.(is_home()?'posts':'single').'_slogan_tag'); ?>
	<<?php echo $slogan; ?> class="wotm-slogan"><?php bloginfo('description'); ?></<?php echo $slogan; ?>>
<?php endif; ?>



<a href="/wrench-of-the-monkey" target="_self" class="wotm-logo wotm-logo-302571251">
    <img src="<?php bloginfo('template_url'); ?>/images/logo-302571251.png" alt="" />
</a><a href="http://supremecenter.com/login/" target="_blank" class="wotm-logo wotm-logo-1204478179">
    <img src="<?php bloginfo('template_url'); ?>/images/logo-1204478179.png" alt="" />
</a><a href="/clients" target="_blank" class="wotm-logo wotm-logo-887308264">
    <img src="<?php bloginfo('template_url'); ?>/images/logo-887308264.png" alt="" />
</a>

<?php if (theme_get_option('theme_use_default_menu')) { wp_nav_menu( array('theme_location' => 'primary-menu') );} else { ?><nav class="wotm-nav">
    <div class="wotm-nav-inner">
    <?php
	echo theme_get_menu(array(
			'source' => theme_get_option('theme_menu_source'),
			'depth' => theme_get_option('theme_menu_depth'),
			'menu' => 'primary-menu',
			'class' => 'wotm-hmenu'
		)
	);
	get_sidebar('nav'); 
?> 
        </div>
    </nav><?php } ?>

                    
